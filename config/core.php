<?php

return [

    /**
     *  Use Hashid for Database table id
     */
    'Hashid' => [
        'key'         => 'f8ca9cce5531baa160838044ab0533118a85604a',
        'length'      => 23,
        'characters'  => 'abcdefghijklmnopqrstuvwxyz',
    ],
];