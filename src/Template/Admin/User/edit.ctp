<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <h3 class="card-header"><i class="fa fa-pencil-square-o"></i>&nbsp; Edit User</h3>
            <div class="card-block">
            <?php 
                echo $this->Flash->render();
                
                $this->Form->setTemplates([
                    'inputContainer' => '<div class="form-group{{required}}"> {{content}} <span class="help">{{help}}</span></div>',
                    'input' => '<input type="{{type}}" name="{{name}}" class="form-control form-control-danger" {{attrs}}/>',
                    'inputContainerError' => '<div class="form-group has-danger {{type}}{{required}}">{{content}}{{error}}</div>',
                    'error' => '<div class="text-danger">{{content}}</div>',
                    'textarea' => '<textarea  name="{{name}}" class="form-control" {{attrs}}>{{value}}</textarea>',
                ]);
                
                echo $this->Form->create($user_data);
                echo $this->Form->controls(
                    [
                        'name' => ['required'  => false, 'placeholder' => 'Enter Name', 'label' => ['class'=> 'form-control-label', 'text' => 'Full Name']],
                        'role' => ['required'  => false, 'placeholder' => 'Enter User Role', 'label' => ['class'=> 'form-control-label', 'text' => 'User Role']],
                        'phone' => ['required'  => false, 'placeholder' => 'Enter User Phone', 'label' => ['class'=> 'form-control-label', 'text' => 'User Phone']],
                        'zipcode' => ['required'  => false, 'placeholder' => 'Enter User Zipcode', 'label' => ['class'=> 'form-control-label', 'text' => 'User Zipcode']],
                    ],
                    [ 'legend' => false]
                );
                echo $this->Form->button('<i class="fa fa-pencil"></i> Update User',['class' => 'btn btn-warning']);
                echo $this->Form->end(); 
            ?>
            </div>
        </div>
    </div>
</div>
