<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <h3 class="card-header"><i class="fa fa-cogs"></i>&nbsp; Manage Articles</h3>
            <div class="card-block">
                <?= $this->Flash->render(); ?>
                <?php if(!empty(iterator_count($articles))) { ?>
                <table class="table table-striped table-hover table-bordered">
                    <thead class="thead-inverse">
                        <tr>
                        <th>#</th>
                        <th>User Full Name</th>
                        <th>User Type</th>
                        <th>User Email</th>
                        <th>Article Title</th>
                        <th>Article Body</th>
                        <th>Insert Date</th>
                        <th>Update Date</th>
                        <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $i = 1;
                            foreach ($articles as $key => $value) { 
                        ?>
                            <tr>
                                <td><?= $i++ ?></td>
                                <td><?= $value->user['name'] ?></td>
                                <td><?= $value->user['role'] ?></td>
                                <td><?= $value->user['email'] ?></td>
                                <td><?= $value->article['title'] ?></td>
                                <td><?= $value->article['body'] ?></td>
                                <td><?= $value->article['created'] ?></td>
                                <td><?= $value->article['modified'] ?></td>
                                <td>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <?php
                                            echo $this->Html->link('<i class="fa fa-pencil-square-o"></i> Edit', '/admin/article/edit/'.$value->EId,['class' => 'btn btn-success', 'escape' => false]);
                                            echo $this->Form->postLink('<i class="fa fa-recycle"></i> Delete', '/admin/article/delete/'.$value->EId,['confirm' => 'Are you sure?', 'class' => 'btn btn-danger', 'escape' => false]);
                                        ?>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table> 
                <?php }else{ ?>
                    <?= $this->Html->image('no-results.svg', ['alt' => 'No Data Found.','style' => 'height: 250px; width: 100%; display: block;']) ?>
                    <h2 style="text-align:center;"><b style="color:red;">Sorry? </b> No Data Found!</h2>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
