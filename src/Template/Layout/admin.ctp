<!DOCTYPE html>
<html lang="en">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $title ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css(['/_assets/bootstrap/css/bootstrap.min.css','font-awesome.min.css']) ?>
    <?= $this->Html->script(['jquery-3.2.1.slim.min.js','popper.min.js','bootstrap.min.js']) ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>

<body>
    <nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="<?= $this->Url->build('/admin') ?>">Admin Panel</a>
        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-list-alt"></i>&nbsp; Article</a>
                    <div class="dropdown-menu">
                        <?=  $this->Html->link('<i class="fa fa-plus-circle"></i>&nbsp; Add Article', '/admin/article/add', ['class' => 'dropdown-item', 'escape' => false]); ?>
                        <?=  $this->Html->link('<i class="fa fa-cogs"></i>&nbsp; Manage Article', '/admin/article/manage', ['class' => 'dropdown-item', 'escape' => false]); ?>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-users"></i>&nbsp; Users</a>
                    <div class="dropdown-menu">
                        <?=  $this->Html->link('<i class="fa fa-cogs"></i>&nbsp; Manage Users', '/admin/user/manage', ['class' => 'dropdown-item', 'escape' => false]); ?>
                    </div>
                </li>
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <?= $this->Html->link('<i class="fa fa-user"></i> Profile', '/admin/user/profile', ['class' => 'nav-link', 'escape' => false]); ?>
                </li>
                <li class="nav-item">
                    <?= $this->Html->link('<i class="fa fa-power-off"></i> Logout', '/Logout', ['class' => 'nav-link', 'escape' => false]); ?>
                </li>
            </ul>
        </div>
    </nav><br>
    <div class="container">
        <?= $this->fetch('content') ?>
    </div>
</body>
</html>