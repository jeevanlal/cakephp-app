<div class="row">
    <div class="col-lg-3">
        <?= $this->Html->image('user.jpg', ['alt' => 'User image','style' => 'height: 250px; width: 100%; display: block;']); ?>
        <br>
        <ul class="list-group">
            <li class="list-group-item">
                <span class="tag tag-default tag-pill float-xs-right"><b>Email ID: &nbsp;</b></span>
                   <?= $user_session['email']; ?>
            </li>
        </ul>
        <br>
    </div>
    <div class="col-lg-9">
        <div class="card">
            <h3 class="card-header">User Profile</h3>
            <div class="card-block">
                <?php 
                    echo $this->Flash->render();
                    
                    $this->Form->setTemplates([
                        'inputContainer' => '<div class="form-group{{required}}"> {{content}} <span class="help">{{help}}</span></div>',
                        'input' => '<input type="{{type}}" name="{{name}}" class="form-control form-control-danger" {{attrs}}/>',
                        'inputContainerError' => '<div class="form-group has-danger {{type}}{{required}}">{{content}}{{error}}</div>',
                        'error' => '<div class="text-danger">{{content}}</div>'
                    ]);
                    
                    echo $this->Form->create($user_data);
                    echo $this->Form->controls(
                        [
                            'name'      => ['value' => $user_session['name'], 'required'  => false, 'placeholder' => 'Enter Full Name', 'label' => ['class'=> 'form-control-label', 'text' => 'User Full Name']],
                            'phone'     => ['value' => $user_session['phone'], 'required'  => false, 'placeholder' => 'Enter Phone', 'label' => ['class'=> 'form-control-label']],
                            'zipcode'   => ['value' => $user_session['zipcode'], 'required'  => false, 'placeholder' => 'Enter Zipcode', 'label' => ['class'=> 'form-control-label']],
                        ],
                        [ 'legend' => false]
                    );
                    echo $this->Form->button('<i class="fa fa-pencil"></i> Update Profile',['class' => 'btn btn-success']);
                    echo $this->Form->end(); 
                ?>
            </div>
        </div>
    </div>
</div>