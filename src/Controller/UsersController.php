<?php
namespace App\Controller;

use Cake\I18n\Time;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

class UsersController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // allow this route/action
        $this->Auth->allow(['Signup']);

        // load the Captcha component and set its parameter
        $this->loadComponent('CakeCaptcha.Captcha', [
            'captchaConfig' => 'login'
        ]);
    }

    public function login()
    {
        if($this->Auth->user('id'))
        {
            // user already login
            return $this->redirect($this->Auth->redirectUrl());
        }else{
            
            // use form validation
            $login = $this->Users->newEntity();
            if($this->request->is('post') AND !empty($this->request->getData()) )
            {
                 // validate the user-entered Captcha code
                $isHuman = captcha_validate($this->request->data['CaptchaCode']);

                // clear previous user input, since each Captcha code can only be validated once
                unset($this->request->data['CaptchaCode']);

                $check_login = $this->Users->patchEntity($login, $this->request->getData(), [
                    'validate' => 'login'
                ]);
    
                if($check_login->errors())
                {
                    // Form Validation TRUE
                    $this->Flash->error('Please Fill required fields');
                }else
                {
                    if ($isHuman) {
                        // Captcha validation passed
                        // Use Auth 
                        $user = $this->Auth->identify();
                        if ($user) {
                            $this->Auth->setUser($user);
                            // $this->Flash->success(__('Login Success.'));
                            if($this->Auth->user('role') == 'admin') {
                                return $this->redirect('/admin');
                            }else{
                                return $this->redirect($this->Auth->redirectUrl());
                            }
                        } else {
                            $this->Flash->error(__('Username or password is incorrect'));
                        }
                    } else {
                        // Captcha validation failed, return an error message
                        $this->Flash->error(__('CAPTCHA validation failed.'));
                    }
                }
            }
            $this->set('login', $login);
        }
    }

    public function signup()
    {
        $sign_up = $this->Users->newEntity();

        if($this->request->is('post') AND !empty($this->request->getData()) )
        {
            
            $signup = $this->Users->patchEntity($sign_up, $this->request->getData(), [
                'validate' => 'sign_up'
            ]);
                
            if($signup->errors())
            {
                // Form Validation TRUE
                $this->Flash->error('Please Fill required fields');
            }else
            {
                $sign_up->name      = $this->request->getData('name');
                $sign_up->username  = $this->request->getData('username');
                $sign_up->password  = $this->request->getData('password');
                $sign_up->email     = $this->request->getData('email');
                $sign_up->phone     = $this->request->getData('phone');
                $sign_up->zipcode   = $this->request->getData('zipcode');
                $sign_up->role      = 'user';

                // Form Validation FALSE
                if($this->Users->save($sign_up))
                {
                    $this->Flash->success('User Added Successfully');
                    return $this->redirect(['action' => 'Signup']);
                }
                $this->Flash->error(__('Unable to add your user!'));
            }
        }
        $this->set('sign_up', $sign_up);
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    /**
     *  User Profile
     */
    public function profile()
    {
        // set title
        $this->set('title', 'Profile');
        // Set the layout.
        $this->viewBuilder()->setLayout('user');

        $this->set('user_session', $this->request->session()->read('Auth.User'));

        /**
         * User Profile Update
         */
        $Users = TableRegistry::get('Users');
        
        $user_data = $Users->get($this->request->session()->read('Auth.User.id'));
        if($this->request->is('put') AND !empty($this->request->getData()) )
        {
            // $user_data->accessible('email', FALSE);

            $userdata = $Users->patchEntity($user_data, $this->request->getData(), [
                'validate' => 'update_profile'
            ]);
            
            if($userdata->errors())
            {
                // Form Validation TRUE
                $this->Flash->error('Please Fill required fields');
            }else
            {
                $user_data->name    = $this->request->getData('name');
                $user_data->phone   = $this->request->getData('phone');
                $user_data->zipcode = $this->request->getData('zipcode');
            
                // Form Validation FALSE
                if($Users->save($user_data))
                {
                    // User Session Update
                    $this->request->session()->write('Auth.User.name', $user_data['name']);
                    $this->request->session()->write('Auth.User.phone', $user_data['phone']);
                    $this->request->session()->write('Auth.User.zipcode', $user_data['zipcode']);
                    $this->redirect('/User/Profile');
                    $this->Flash->success('User has been Updated.');
                }else{
                    $this->Flash->error(__('Unable to update user!'));
                }
            }
        }
        $this->set(compact('user_data'));
        $this->set('_serialize', ['user_data']);
    }
}