<?php

namespace App\Controller;

class BlogController extends AppController
{
    public function blog()
    {
        // set title
        $this->set('title', 'Welcome to Blog');
        // Set the layout.
        $this->viewBuilder()->setLayout('user');

        $this->set('user_session', $this->request->session()->read('Auth.User'));
    }
}
?>