<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\Utility\Security;
use Hashids\Hashids;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;


class UserController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function index()
    {
        $this->set('title', 'View All Users');

    }

    public function profile()
    {
        $this->set('title', 'User Profile');
        
    }

    public function manage()
    {
        $this->set('title', 'Manage Uses');
        // Fetch All Users
        $user = TableRegistry::get('Users');
        $users = $user->find('all');
        $this->set(compact('users'));
    }

    /**
     * User Edit
     * @param : user_id
     */
    public function edit($eid = null)
    {
        $this->set('title', 'User Edit');

        $User = TableRegistry::get('Users');
        /**
         *  Set Hashids Configure and DecodeHex
         */
        $hashids = new Hashids(Configure::read('Hashid.key'), Configure::read('Hashid.length'), Configure::read('Hashid.characters'));
        $id = $hashids->decodeHex($eid);

        // get user
        $user_data = $User->get($id);

        // update user
        if($this->request->is('put') AND !empty($this->request->getData()))
        {
            $user_data->accessible('user_id', FALSE);
            $user_data->accessible('id', FALSE);

            $update_user = $User->patchEntity($user_data, $this->request->getData(), [
                'validate' => 'update'
            ]);
            
            // check validation errors
            if($update_user->errors())
            {
                $this->Flash->error(__('Please Fill required fields'));
            }else
            {
                $update_user->name  = $this->request->getData('name');
                $update_user->role  = $this->request->getData('role');
                $update_user->phone = $this->request->getData('phone');
                $update_user->zipcode = $this->request->getData('zipcode');

                // Form Validation FALSE
                if($User->save($update_user))
                {
                    // update success
                    $this->Flash->success(__('User has been Updated.'));
                }else{
                    // update server error
                    $this->Flash->error(__('Unable to update user!'));
                }
            }
        }

        // set data in template
        $this->set(compact('user_data'));
        $this->set('_serialize', ['user_data']);
    }

    /**
     * User Delete
     * @param : user_id
     */
    public function delete($eid = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $User = TableRegistry::get('Users');
        /**
         *  Set Hashids Configure and DecodeHex
         */
        $hashids = new Hashids(Configure::read('Hashid.key'), Configure::read('Hashid.length'), Configure::read('Hashid.characters'));
        $id = $hashids->decodeHex($eid);

        // get user
        $user_data = $User->get($id);

        if ($User->delete($user_data)) 
        {
            $this->Flash->success(__('The user with id: {0} has been deleted.', h($id)));
            return $this->redirect('/admin/user/manage');
        }
    }
}