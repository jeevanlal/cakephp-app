<?php

namespace App\Model\Entity;
use Cake\ORM\Entity;
use Cake\Utility\Security;
use Cake\Core\Configure;
use Hashids\Hashids;


class Article extends Entity
{
    // protected $_accessible = [
    //     ' * ' => true,
    //     'id'    => false,
    //     'user_id' => false,
    // ];


    protected function _getEId()
    {
        // load hashid config
        $hashids = new Hashids(Configure::read('Hashid.key'), Configure::read('Hashid.length'), Configure::read('Hashid.characters'));

        if(!isset($this->_properties['article']['id']))
            return $hashids->encodeHex($this->_properties['id']);
        else
            return $hashids->encodeHex($this->_properties['article']['id']);
    }
}