<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\RulesChecker;

class ArticleTable extends Table
{
    public function initialize(array $config) 
    {
        $this->setTable('articles');
        $this->addBehavior('Timestamp');
    }

    // public function buildRules(RulesChecker $rules)
    // {
    //     // $rules->add($rules->isUnique(['email'], 'Email ID Already Exists'));
    //     // return $rules;
    // }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->notEmpty('title')
            ->lengthBetween('title', [8, 100], 'Allow only Min 8 and Max 100 characters')
            ->notEmpty('body')
            ->lengthBetween('body', [8, 1000], 'Allow only Min 8 and Max 1000 characters');
            
        return $validator;
    }

    /**
     *  User Profile Update
     */
    public function validationUpdate_article($validator)
    {
        $validator
            ->notEmpty('title')
            ->lengthBetween('title', [8, 100], 'Allow only Min 8 and Max 100 characters')
            ->notEmpty('body')
            ->lengthBetween('body', [8, 100], 'Allow only Min 8 and Max 100 characters');
            
        return $validator;
    }
}